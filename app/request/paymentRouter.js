const router = require("express").Router();
const paystack = require("paystack")(
  "sk_test_6ad3199869b40e00987022b9dd3c7be0709367be"
);
const request = require("request");

//to initialize a transaction
router.post("/initialize", (req, res) => {
  finalAmt = req.body.amount * 100;
  paystack.transaction.initialize(
    { amount: finalAmt, email: req.body.email },
    (error, body) => {
      res.send(body.data);
    }
  );
});

//for admin to get all the info related to payments
router.get("/transactions", (req, res) => {
    paystack.transaction.list((err, body) => {
        return res.send(body.data);
      });
});

//as the route name implies
router.get("/totals", (req, res) => {
  paystack.transaction.totals((err, body) => {
    res.send(body.data);
  });
});

//all the cusomers on the platform
router.get("/customers", (req, res) => {
  paystack.customer.list((err, body) => {
    res.send(body.data);
  });
});

//get customers per page
router.get("/page/:id", (req, res) => {
  paystack.customer.get((err, body) => {
    res.send(body.data);
  });
});

//verify that a transaction ref has not been used before
router.post("/verify", (req, res) => {
  let q = req.body;
  paystack.transaction.verify(q.ref, (err, body) => {
    if (err) res.send({ response: "invalid transaction" });
    else if (body.data.status == "success") res.send({ response: "success" });
    else res.send({ response: "Abandoned" });
    console.log("the status is " + body.data.status);
  });
});

module.exports = router;
