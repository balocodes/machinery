const router = require("express").Router();
const cloudinary = require("cloudinary");
const formidable = require("formidable");
const jwt = require("jsonwebtoken");
let errorOccured = false;

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET
});

// router.use("*", (req, res, next) => {
//   let token = req.body.token || req.query.token;
//   try {
//     req.decoded = jwt.verify(token, process.env.ADMIN_JWT_SECRET);
//     next();
//   } catch (error) {
//     res.send({
//       message: "Verification failed. Please login again.",
//       code: 401,
//       error: true
//     });
//   }
// }); //done

router.post("/upload", async (req, res) => {
  // console.log(req.query.token)
  const form = await new formidable.IncomingForm();
  form.parse(req, async function(err, fields, files) {
    try {
      // switch (req.query.who) {
      //   case "user":
      //     console.log("who is user");
      //     jwt.verify(req.query.token, process.env.USER_JWT_SECRET);
      //     break;

      //   case "admin":
      //     jwt.verify(fields.token, process.env.ADMIN_JWT_SECRET);
      //     break;

      //   default:
      //     return res.send({
      //       message: "Please indicate who is uploading the file.",
      //       error: true
      //     });
      // }
    } catch (error) {
      errorOccured = true;
    } finally {
      if (!errorOccured) {
        console.log("Uploading...");
        cloudinary.uploader.upload(files.files.path, result => {
          res.send(result);
        });
      } else {
        res.send({
          message: "Verification failed. Please sign in again.",
          error: true
        });
      }
    }
  });
});

module.exports = router;
