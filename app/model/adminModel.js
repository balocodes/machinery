const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
  fname: { type: String, required: true },
  lname: { type: String, required: true },
  email: { type: String, required: true },
  phone: { type: String, required: true },
  password: { type: String, required: true },
  access_level: { type: Number, default: 0 },
  status: { type: Boolean, default: true },
  created_at: { type: Date, default: current_time },
  updated_at: { type: Date, default: Date.now }
});

// new_schema.index({
// });

const model = mongoose.model("Admin", new_schema)

module.exports = model