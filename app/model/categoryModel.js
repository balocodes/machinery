const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
  category_name: { type: String, required: true, unique: true},
  category_image: { type: String },
  about: { type: String },
  status: { type: Boolean, default: true },
  image: { type: String },
  created_at: { type: Date, default: current_time },
  updated_at: { type: Date, default: Date.now }
});

// new_schema.index({
// });

const model = mongoose.model("Category", new_schema)

module.exports = model