const mongoose = require("mongoose");
let current_time = new Date().toISOString();

let new_schema = new mongoose.Schema({
  company_name: { type: String, required: true },
  email: { type: String, required: true },
  phone: { type: String },
  password: { type: String },
  profile_image: { type: String },
  about: { type: String },
  certificate: { type: String },
  verified: { type: Boolean, default: false },
  location: { type: String },
  address: { type: String },
  subscription_package: { type: String, default: "None" },
  subscription_start_date: { type: Date },
  subscription_end_date: { type: Date },
  status: { type: Boolean, default: true },
  recovery_url: { type: String },
  created_at: { type: Date, default: current_time },
  updated_at: { type: Date, default: Date.now }
});

// new_schema.index({
// });

const model = mongoose.model("Owner", new_schema)

module.exports = model