const subModel = require("../model/subscriptionPackageModel");
const voucherModel = require("../model/voucherModel");
const paystack = require("paystack")("sk_test_6ad3199869b40e00987022b9dd3c7be0709367be");


class PaymentCtrl {
    constructor() {

    }

    // This initializes a payment and returns the link to the paystack payment
    // page.
    initiatePayment(req, res) {
        let voucher_percentage = 0;
        let voucher_fixed = 0;

        // check if subscription package exists
        subModel.findById(req.body.sub_id, (sub_err, sub_data) => {
            if (sub_err) {
                res.send({
                    message: "An error occured while trying to find subscription",
                    code: 500,
                    error: true
                })
            } else if (sub_data != null) {
                let finalAmt = Number(sub_data.price) * 100
                //check if a voucher is used
                if (req.body.voucher_code) {
                    voucherModel.findOne({
                        voucher_code: req.body.voucher_code
                    }, (v_err, v_data) => {
                        if (v_err) {
                            res.send({
                                message: "An error occured with the voucher"
                            })
                        } else if (v_data) {

                            voucher_percentage = v_data.voucher_discount;
                            voucher_fixed = v_data.voucher_fixed

                            // check if the voucher type is fixed amount or percentage
                            if (voucher_fixed != null) {
                                finalAmt = finalAmt - Number(voucher_fixed)
                            } else {
                                finalAmt = finalAmt - (finalAmt * (Number(voucher_percentage) / 100))
                            }

                            paystack.transaction.initialize({
                                    amount: finalAmt,
                                    email: req.body.email
                                },
                                (error, body) => {
                                    if (error) {
                                        res.send({
                                            "message": "An error occured while initializing payment",
                                            "code": 500,
                                            "error": true
                                        })
                                    } else {
                                        res.send({
                                            result: body.data,
                                            message: "Initialization successful",
                                            code: 200,
                                            error: false
                                        })
                                    }

                                }
                            );
                        } else {
                            return res.send({
                                message: "Invalid Voucher",
                                code: 401,
                                error: true
                            })
                        }
                    })
                } else {
                    paystack.transaction.initialize({
                            amount: finalAmt,
                            email: req.body.email
                        },
                        (error, body) => {
                            if (error) {
                                res.send({
                                    "message": "An error occured while initializing payment",
                                    "code": 500,
                                    "error": true
                                })
                            } else {
                                res.send({
                                    result: body.data,
                                    message: "Initialization successful",
                                    code: 200,
                                    error: false
                                })
                            }
                        }
                    );
                }
            } else {
                res.send({
                    message: "Subscription package not found!"
                })
            }
        })


    }
}

module.exports = PaymentCtrl;