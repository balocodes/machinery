const filter = require("./misc").objectFilter
class GeneralCtrl {
  constructor(model, res = null, mode = "development") {
    this.model = model;
    this.res = res;
    this.mode = mode;
    this.error = {
      message: "An error occured!",
      code: 500,
      error: true
    };
  }

  /**
   * Checks the mode of the app and decides whether to log data
   * passed into it or not.
   * @param {Object} toLog
   */
  checkMode(toLog) {
    if (this.mode == "development") {
      console.log(toLog);
    }
    return true;
  }

  /**
   * Checks the `data` property of the `obj` passed into it
   * and adds the content to the database
   * @param {Object} obj
   */
  addItem(obj) {
    let toAdd = new this.model(obj.data);
    toAdd.save((err, product) => {
      if (err) {
        this.checkMode(err);
        return this.res.send(this.error);
      } else {
        return this.res.send({
          result: product,
          message: "Data added successfully",
          code: 200,
          error: false
        });
      }
    });
  }

  /**
   * Checks for the `data` property of the object and
   * uses it as a search parameter if it exists, otherwise
   * it fetches all data base on the other properties of
   * the object which include `limit`, `page`, `order`, `fields`
   * @param {Object} obj
   */
  readItems(obj) {
    this.model
      .find(obj.data)
      .limit(Number(obj.limit))
      .skip(Number(obj.page) * Number(obj.limit))
      .sort(obj.order)
      .select(obj.fields)
      .exec((err, data) => {
        if (err) {
          this.checkMode(err);
          return this.res.send(this.error);
        } else {
          return this.res.send({
            result: data,
            message: "Query successful",
            code: 200,
            error: false
          });
        }
      });
  }

  /**
   * This checks the `data` property of an `obj` and
   * uses it to find a single document that matches
   * that `data`
   * @param {Object} obj
   */
  readSingleItem(obj) {
    this.model.findOne(obj.data, obj.fields, (err, result) => {
      if (err) {
        this.checkMode(err);
        return this.res.send(this.error);
      } else {
        this.checkMode("single item: ", result);
        return this.res.send({
          result: result,
          message: "Query successful",
          code: 200,
          error: false
        });
      }
    });
  }

  /**
   * Searches for documents based on the index in their
   * models. Requires the `search` property of `obj`
   * @param {Object} obj
   */
  search(obj) {
    let sort;
    if (obj.order == null) {
      sort = { score: { $meta: "textScore" } };
    } else {
      sort = obj.order;
    }
    this.model
      .find(
        { $text: { $search: obj.search } },
        { score: { $meta: "textScore" } }
      )
      .limit(Number(obj.limit))
      .skip(Number(obj.page) * Number(obj.limit))
      .select(obj.fields)
      .sort(sort)
      .exec((err, data) => {
        if (err) {
          this.checkMode(err);
          this.res.send(this.error);
        } else {
          this.res.send({
            result: data,
            message: "Query successful",
            code: 201,
            error: false
          });
        }
      });
  }

  /**
   * Finds a document using the `find` property of the `obj`
   * and then updates the object with the `data` properties
   * of `obj`
   * @param {Object} obj
   */
  updateItem(obj) {
    this.model.findByIdAndUpdate(obj.find, obj.data, (err, data) => {
      if (err) {
        this.checkMode(err);
        return this.res.send(this.error);
      } else {
        this.checkMode({ previous_data: data });
        this.res.send({
          message: "Update successful",
          code: 201,
          error: false
        });
      }
    });
  }

  /**
   * Uses the `data._id` property of `obj` to find an object and
   * remove the document that corresponds to that `_id`
   * @param {Object} obj
   */
  deleteItem(obj) {
    this.model.findByIdAndRemove(obj.data._id, (err, data) => {
      if (err) {
        this.checkMode(err);
        this.res.send(this.error);
      } else {
        this.res.send({
          message: "Delete successful",
          code: 201,
          error: false
        });
      }
    });
  }
  /**
   * Return the number of documents based on the data given
   * @param {Object} obj 
   */
  count(obj) {
    this.model.count(obj.data, (err, count) => {
      if(err) {
        res.send({
          message: "An error occured!",
          code: 500,
          error: true
        })
      } else {
        this.res.send({
          result: count,
          message: "Query successful!",
          code: 200,
          error: false
        })
      }
    })
  }
}

module.exports = GeneralCtrl;
