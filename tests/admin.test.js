const request = require("request");
let base_url = "http://localhost:3333/admin/";
let routes = {
  test: "",
  add_machine: "machine",
  read_machines: "machines",
  delete_machine: "machine",
  single_machine: "machine",
  update_machine: "machine"
};

let machine = {
  data: {
    name: "Test LLLLLL",
    owner_id: null,
    description: "Just a test machine",
    category: "Tests",
    features: { price: 2000 },
    image:
      "https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/rea-free-template.jpg",
    price: "14000000",
    location: "Abuja",
    address: "Dawaki, Abuja",
    contact_phone: "07011636330",
    contact_email: "balocodes@gmail.com"
  }
};

let test_item = "";

it.only("should return true", done => {
  expect.assertions(1);
  request(
    `${base_url}${routes.test}`,
    { form: {}, method: "GET" },
    (err, resp, body) => {
      console.log(`${base_url}${routes.test}`);
      expect(resp.body).toBeTruthy();
      done();
    }
  );
});

//note that we need to stringify objects to send them through request module
it("should add a machine", done => {
  expect.assertions(1);
  request(
    `${base_url}${routes.add_machine}`,
    { form: { data: JSON.stringify(machine.data) }, method: "POST" },
    (err, resp, body) => {
      if (JSON.parse(resp.body).result) {
        console.log(JSON.parse(resp.body).message);
        test_item = JSON.parse(resp.body).result._id;
      }
      expect(JSON.parse(resp.body).error).toBeFalsy();
      done();
    }
  );
});

it("should read added machine", done => {
  expect.assertions(1);
  console.log("test_item: ", test_item);
  request(
    `${base_url}${routes.single_machine}`,
    { form: { data: JSON.stringify({ _id: test_item }) }, method: "GET" },
    (err, resp, body) => {
      console.log(JSON.parse(resp.body).message);
      expect(JSON.parse(resp.body).error).toBeFalsy();
      done();
    }
  );
});

it("should update added machine", done => {
  request(
    `${base_url}${routes.update_machine}`,
    {
      form: {
        find: JSON.stringify({ _id: test_item }),
        data: JSON.stringify({ name: "Updated cool machine" })
      },
      method: "PUT"
    },
    (err, resp, body) => {
      console.log(JSON.parse(resp.body).message);
      expect(JSON.parse(resp.body).error).toBeFalsy();
      done();
    }
  );
});

it("should delete added machine", done => {
  request(
    `${base_url}${routes.delete_machine}`,
    { form: { data: JSON.stringify({ _id: test_item }) }, method: "DELETE" },
    (err, resp, body) => {
      console.log(JSON.parse(resp.body).message);
      expect(JSON.parse(resp.body).error).toBeFalsy();
      done();
    }
  );
});
