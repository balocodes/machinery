const userRouter = require("../app/request/userRouter");
const request = require("request");
const base_url = "http://localhost:3333/user/";
const user = {
  data: {
    company_name: "Balo Machineries",
    email: "balo@projaro.com",
    phone: "07011636330",
    password: "amin"
  },
  find: {
    email: "balo@projaro.com"
  }
};

let test_item;

it.only("should register an owner", done => {
  expect.assertions(1);
  request(
    `${base_url}register`,
    {
      form: {
        data: JSON.stringify(user.data),
        find: JSON.stringify(user.find)
      },
      method: "POST"
    },
    (err, resp, body) => {
      // console.log("resp: ", resp);
      console.log("body: ", body)
      if (JSON.parse(resp.body).result) {
        console.log(JSON.parse(resp.body).message);
        test_item = JSON.parse(resp.body).result._id;
      }
      expect(JSON.parse(resp.body).error).toBeFalsy();
      done();
    }
  );
});

it("should delete an owner", done => {
  expect.assertions(1);
  request(
    `${base_url}profile`,
    {
      form: {
        data: JSON.stringify({data: {test_item: test_item}}),
        find: JSON.stringify(user.find)
      },
      method: "DELETE"
    },
    (err, resp, body) => {
      // console.log("resp: ", resp);
      console.log("body: ", body)
      if (JSON.parse(resp.body).result) {
        console.log(JSON.parse(resp.body).message);
        test_item = JSON.parse(resp.body).result._id;
      }
      expect(JSON.parse(resp.body).error).toBeFalsy();
      done();
    }
  );
});
